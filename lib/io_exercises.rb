# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
    puts "Welcome to the guessing game!"
    #generate random number between 1 and 100 inclusive
    num_to_guess = random_num_between_1_and_100
    guesses = []
    #prompt user to guess number

    while true
      puts "Please guess a number between 1 and 100"
      input = gets.chomp.to_i
      #record user inputs (guesses)
      guesses << input

      #perform data validation on user input
        #must be an intiger
        #must be between 1 and 100 inclusive
      if valid_input?(input)
        if input == num_to_guess
          break
        end
        #tells user if they guessed too high or too low
        user_guider(input, num_to_guess)
        #show all guesses after each guess is made
        print_guesses(guesses)
      else
        user_guider(input, num_to_guess)
        puts "Error: user input is outside of range."
      end

    end
    #when user guesses number correctly print the number and how many guesses
    you_win!(input, guesses)
end

def random_num_between_1_and_100
  prng = Random.new
  prng.rand(1..100)
end

def valid_input?(input)
  is_integer(input) && is_between_1_and_100(input)
end

def is_integer(num)
  num == Integer(num)
end

def is_between_1_and_100(num)
  num >= 1 && num <= 100
end

def you_win!(input, guesses)
  puts "You win!!"
  puts input
  puts "You guessed the number in #{guesses.length} tries!"
end

def user_guider(input, num_to_guess)
  if input > num_to_guess
    puts "You guessed too high"
  else
    puts "You guessed too low"
  end
end

def print_guesses(guesses)
  list_of_guesses = guesses.join(" ")
  puts "#{list_of_guesses}"
end
########################################################################
